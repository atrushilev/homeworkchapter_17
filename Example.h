#include <iostream>
using namespace std;

class Example
{
private:
	int a = 5;
	int b = 6;
public:
	void setAB(int _a, int _b)
	{
		a = _a;
		b = _b;
	}
	void getAB()
	{
		cout << a << " " << b << endl;
	}
};
