﻿// HomeWorkChapter_17.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "Example.h"
#include "Vector.h"

int main()
{
	int a, b;
	cout << "First variable of the class: "; cin >> a;
	cout << "Second variable of the class: "; cin >> b;
	cout << endl << "Variables of the class: " << endl;
	Example ex;
	Example ex1;
	Vector v(-1, 2, 3);
	Vector v1; // конструктор Vector() по умолчанию
	ex.setAB(a, b); // инициализация a, b
	ex.getAB(); // вывод инициализированных a, b
	ex1.getAB(); // вывод приватных a, b класса Example
	cout << endl;
	cout << "Vector modules: " << endl;
	cout << v.VectorModule() << endl;
	cout << v1.VectorModule() << endl;
}



