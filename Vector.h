#include <iostream>
#include <cmath>
using namespace std;

class Vector
{
private:
	double x;
	double y;
	double z;
public:
	Vector() : x(0), y(0), z(0)
	{}
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}
	double VectorModule()
	{
		return sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
	}
};
